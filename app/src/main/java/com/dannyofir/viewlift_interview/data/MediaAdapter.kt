package com.dannyofir.viewlift_interview.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dannyofir.viewlift_interview.R
import com.dannyofir.viewlift_interview.networking.response.MediaItem
import kotlinx.android.synthetic.main.media_item_layout.view.*
import kotlin.math.roundToInt

class MediaAdapter (private val mediaItemListener: MediaItemListener) :
    ListAdapter<MediaItem, RecyclerView.ViewHolder>(MediaCallback()) {

    class MediaCallback : DiffUtil.ItemCallback<MediaItem>() {

        override fun areItemsTheSame(oldItem: MediaItem, newItem: MediaItem): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: MediaItem, newItem: MediaItem): Boolean {
            return oldItem.title == newItem.title
        }

    }

    interface MediaItemListener {
        fun onMediaItemClicked(videoUrl: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.media_item_layout, parent, false)
        val holder = MediaItemViewHolder(view)
        holder.itemView.setOnClickListener {
            mediaItemListener.onMediaItemClicked(getItem(holder.adapterPosition).media.url)
        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mediaItem = getItem(position)
        (holder as MediaItemViewHolder).bind(mediaItem)
    }

    class MediaItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val mediaItemImage: AppCompatImageView = itemView.media_item_image
        val mediaItemTitle: AppCompatTextView = itemView.media_item_title
        val mediaItemDuration: AppCompatTextView = itemView.media_item_duration

        fun bind(mediaItem: MediaItem) {

            Glide.with(mediaItemImage)
                    .load(mediaItem.media.thumbnail.url)
//                    .apply(RequestOptions.circleCropTransform())
                    .into(mediaItemImage)

            mediaItemTitle.text = mediaItem.title
            mediaItemDuration.text = mediaItem.media.duration.roundToInt().toString() + "s"

        }

    }
}