package com.dannyofir.viewlift_interview.data

import com.dannyofir.viewlift_interview.networking.response.FeedResponse
import com.dannyofir.viewlift_interview.networking.service.MediaService
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MediaRepository @Inject constructor(private val mediaService: MediaService) {

    fun getMedia() : Observable<FeedResponse> {
        return mediaService.getMedia().toObservable()
    }

}