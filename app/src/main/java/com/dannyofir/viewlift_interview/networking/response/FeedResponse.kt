package com.dannyofir.viewlift_interview.networking.response

import org.simpleframework.xml.*

@Root(name = "rss", strict = false)
data class FeedResponse (
    @field:ElementList(name = "item", inline = true, required = false)
    @param:ElementList(name = "item", inline = true, required = false)
    @field:Path("channel")
    @param:Path("channel")
    val mediaList: List<MediaItem>
)

