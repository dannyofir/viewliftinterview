package com.dannyofir.viewlift_interview.networking.response

import org.simpleframework.xml.*

@Root(name = "item", strict = false)
data class MediaItem(
    @field:Element(name = "link")
    @param:Element(name = "link")
    val link: String,
    @field:Element(name = "guid")
    @param:Element(name = "guid")
    val guid: String,
    @field:Element(name = "description")
    @param:Element(name = "description")
    val description: String,
    @field:Element(name = "title")
    @param:Element(name = "title")
    val title: String,
    @field:Element(name = "pubDate")
    @param:Element(name = "pubDate")
    val pubDate: String,
    @field:Element(name = "content")
    @param:Element(name = "content")
    val media: MediaDetails
)

@Root(name = "content", strict = false)
data class MediaDetails (
    @field:Attribute(name = "url")
    @param:Attribute(name = "url")
    val url: String,
    @field:Attribute(name = "duration")
    @param:Attribute(name = "duration")
    val duration: Double,
    @field:Element(name = "thumbnail")
    @param:Element(name = "thumbnail")
    val thumbnail: MediaThumbnail
)

@Root(name = "thumbnail", strict = false)
data class MediaThumbnail (
    @field:Attribute(name = "url")
    @param:Attribute(name = "url")
    val url: String
)
