package com.dannyofir.viewlift_interview.networking.service

import com.dannyofir.viewlift_interview.networking.service.MediaService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class MediaServiceModule {

    @Provides
    @Singleton
    fun provideMediaService(retrofit: Retrofit): MediaService {
        return retrofit.create(MediaService::class.java)
    }

}