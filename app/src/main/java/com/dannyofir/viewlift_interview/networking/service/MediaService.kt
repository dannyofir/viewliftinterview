package com.dannyofir.viewlift_interview.networking.service

import com.dannyofir.viewlift_interview.networking.response.FeedResponse
import io.reactivex.Single
import retrofit2.http.GET

interface MediaService {

    @GET("feed_firetv.xml")
    fun getMedia() : Single<FeedResponse>

}