package com.dannyofir.viewlift_interview

import android.app.Application
import com.dannyofir.viewlift_interview.ViewLiftApplication
import com.dannyofir.viewlift_interview.di.ViewModelModule
import com.dannyofir.viewlift_interview.di.module.ActivityBindingModule
import com.dannyofir.viewlift_interview.networking.service.MediaServiceModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        ApplicationModule::class,
        MediaServiceModule::class]
)
interface ApplicationComponent : AndroidInjector<ViewLiftApplication> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun create(application: Application): Builder

        fun build(): ApplicationComponent

    }

}