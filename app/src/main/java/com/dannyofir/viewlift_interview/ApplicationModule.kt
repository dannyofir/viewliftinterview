package com.dannyofir.viewlift_interview

import com.dannyofir.viewlift_interview.networking.NetworkModule
import dagger.Module
import dagger.Provides
import okhttp3.Call
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
class ApplicationModule {

    @Provides
    @Singleton
    fun provideRetrofit(callFactory: Call.Factory, @Named("base_url") baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .callFactory(callFactory)
            .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(
                Persister(AnnotationStrategy())
            ))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(baseUrl)
            .build()
    }

}