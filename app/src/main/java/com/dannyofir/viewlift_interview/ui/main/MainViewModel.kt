package com.dannyofir.viewlift_interview.ui.main

import androidx.lifecycle.MutableLiveData
import com.dannyofir.viewlift_interview.base.BaseViewModel
import com.dannyofir.viewlift_interview.data.MediaRepository
import com.dannyofir.viewlift_interview.networking.response.FeedResponse
import com.dannyofir.viewlift_interview.networking.response.MediaItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mediaRepository: MediaRepository) : BaseViewModel() {

    val mediaList = MutableLiveData<List<MediaItem>>()

    init {
        getMedia()
    }

    fun getMedia() {

        mediaRepository.getMedia()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                compositeDisposable.add(it)
            }
            .subscribe(Consumer {
                mediaList.value = it.mediaList
            }, Consumer {
                Timber.d(it.toString())
            })

    }

}
