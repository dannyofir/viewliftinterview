package com.dannyofir.viewlift_interview.ui.player

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.dannyofir.viewlift_interview.R
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.TransferListener
import com.google.android.exoplayer2.util.Util
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.player_fragment.*
import javax.inject.Inject


class PlayerFragment : DaggerFragment() {

    lateinit var player: ExoPlayer

    companion object {
        fun newInstance() = PlayerFragment()
    }

    private lateinit var viewModel: PlayerViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.player_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PlayerViewModel::class.java)

        val bandwidthMeter = DefaultBandwidthMeter()

        val mediaDataSourceFactory = DefaultDataSourceFactory(activity, Util.getUserAgent(activity, "mediaPlayerSample"),
            bandwidthMeter as TransferListener)

        val simpleExoPlayerView = player_view
        simpleExoPlayerView.requestFocus()

        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)

        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        player = ExoPlayerFactory.newSimpleInstance(activity, trackSelector)

        player.setPlayWhenReady(true)

        simpleExoPlayerView.setPlayer(player)

        val extractorsFactory = DefaultExtractorsFactory()

        val mediaSource = ExtractorMediaSource(
            Uri.parse(activity?.intent?.getStringExtra("videoUrl")),
            mediaDataSourceFactory, extractorsFactory, null, null
        )

        player.prepare(mediaSource)
    }

    override fun onDestroy() {
        player.stop()
        player.release()
        super.onDestroy()
    }

}
