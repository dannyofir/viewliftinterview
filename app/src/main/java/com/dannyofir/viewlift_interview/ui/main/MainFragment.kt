package com.dannyofir.viewlift_interview.ui.main

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import com.azoft.carousellayoutmanager.CenterScrollListener
import com.dannyofir.viewlift_interview.R
import com.dannyofir.viewlift_interview.data.MediaAdapter
import com.dannyofir.viewlift_interview.ui.PlayerActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class MainFragment : DaggerFragment(), MediaAdapter.MediaItemListener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        val carouselLayoutManager = CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL)
        carouselLayoutManager.setPostLayoutListener(CarouselZoomPostLayoutListener())

        media_recycler_view.layoutManager = carouselLayoutManager
        media_recycler_view.setHasFixedSize(true)
        media_recycler_view.addOnScrollListener(CenterScrollListener())

        viewModel.mediaList.observe(this, Observer {

            val mediaAdapter = MediaAdapter(this)
            media_recycler_view.adapter = mediaAdapter
            mediaAdapter.submitList(it)

        })
        // TODO: Use the ViewModel
    }

    override fun onMediaItemClicked(videoUrl: String) {
        val intent = Intent(activity, PlayerActivity::class.java).apply {
            putExtra("videoUrl", videoUrl)
        }
        startActivity(intent)
    }

}
