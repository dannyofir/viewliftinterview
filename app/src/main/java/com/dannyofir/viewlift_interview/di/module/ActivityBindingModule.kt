package com.dannyofir.viewlift_interview.di.module

import com.dannyofir.viewlift_interview.ui.MainActivity
import com.dannyofir.viewlift_interview.ui.PlayerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    abstract fun playerActivity(): PlayerActivity


}