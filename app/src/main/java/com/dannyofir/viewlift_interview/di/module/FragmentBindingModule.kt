package com.dannyofir.viewlift_interview.di.module

import com.dannyofir.viewlift_interview.ui.main.MainFragment
import com.dannyofir.viewlift_interview.ui.player.PlayerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun mainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun playerFragment(): PlayerFragment

}